let btn = document.querySelector('.btn-change-color-theme');
let linkCSS = document.querySelectorAll('link')[1]

btn.addEventListener('click', switchThemeColor);

function switchThemeColor() {
    if (linkCSS.getAttribute('href')==='style-color-theme.css'){
        linkCSS.setAttribute('href', 'style.css');
        localStorage.removeItem('theme'); 
     }else {
        linkCSS.setAttribute('href', 'style-color-theme.css');
        localStorage.setItem('theme', 'black-color-theme'); 
    }
}

window.onload = function() {
    if(localStorage.getItem('theme') === 'black-color-theme'){
        linkCSS.setAttribute('href', 'style-color-theme.css')
    } else {
        linkCSS.setAttribute('href', 'style.css')
    }
}
